package com.canopus.ilimi;
 
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
 
public class GitlabAdapter {
 
  public static void main(String[] args) {
  
  if(args.length <=2){
				System.out.println("Usage: GitlabAdapter token filename");
				return; 
			}
			inputXML = args[0];
			inputXSL = args[1];
			resultXML = args[2];
			
			//inputXML ="/Users/kamlesh/Documents/workspace/XSLTranalator/resourses/cppcheck.xml";
			//inputXSL = "/Users/kamlesh/Documents/workspace/XSLTranalator/resourses/cppcheck.xsl";
			//resultXML = "/Users/kamlesh/Documents/workspace/XSLTranalator/resourses/results.xml";
			
			System.out.println("Processing "+inputXML + " & "+ inputXSL+" To generate :" +resultXML);
 
	ReadCVS obj = new ReadCVS();
	obj.run();
 
  }
 
  public void run() {
 
	String csvFile = "/Users/mkyong/Downloads/GeoIPCountryWhois.csv";
	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ",";
 
	try {
 
		br = new BufferedReader(new FileReader(csvFile));
		while ((line = br.readLine()) != null) {
 
		        // use comma as separator
			String[] country = line.split(cvsSplitBy);
 
			System.out.println("Country [code= " + country[4] 
                                 + " , name=" + country[5] + "]");
 
		}
 
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
 
	System.out.println("Done");
  }
 
}